import { doRequest } from '../../utils/CoreUtils';
import { errorHandler } from '../../utils/StringUtils';

import { product_domain } from '../../constants/Domain';

export default class ProductApi {

    async getProducts({} = {}) {
        try {
            let url = `${product_domain}products`;
            return await doRequest('get', url);
        } catch (error) {
            throw errorHandler(error);
        }
    }

}