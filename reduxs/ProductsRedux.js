import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- START: Types and Action Creators ------------- */
const { Types, Creators } = createActions({
    
    getProductsRequest: ['classify'], // classify: Products
    getProductsSuccess: ['classify', 'payload'],
    getProductsFailure: ['classify', 'error'],

});
export const ProductTypes = Types;
export default Creators;
/* ------------- END: Types and Action Creators ------------- */

/* ------------- START: Initial State ------------- */
export const INITIAL_STATE = Immutable({
    error: {},
    fetching: {},
    content: {},
});
/* ------------- END: Initial State ------------- */

/* ------------- START: getProducts ------------- */
export const getProductsRequest = (state, { classify }) => {
    return state.merge({
        fetching: { ...state.fetching, [classify]: true },
        error: { ...state.error, [classify]: null },
    });
};

export const getProductsSuccess = (state, { classify, payload }) => {
    return state.merge({
        fetching: { ...state.fetching, [classify]: false },
        content: { ...state.content, [classify]: payload },
    });
};

export const getProductsFailure = (state, { classify, payload }) => {
    return state.merge({
        fetching: { ...state.fetching, [classify]: false },
        error: { ...state.error, [classify]: error },
    });
};
/* ------------- END: getProducts ------------- */

/* ------------- START: Hookup Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {


    [Types.GET_PRODUCTS_REQUEST]: getProductsRequest,
    [Types.GET_PRODUCTS_SUCCESS]: getProductsSuccess,
    [Types.GET_PRODUCTS_FAILURE]: getProductsFailure,

});
/* ------------- END: Hookup Reducers To Types ------------- */