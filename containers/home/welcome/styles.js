import { getShadowStyle } from '../../../utils/StyleUtils';

import { cStyles } from '../../../assets/styles';
import { borderRadiuses, fontFamilys,fontWeights, fontSizes, textColors, colors, boxShadows, baseHeights } from '../../../assets/styles/Theme';

export const styles = theme => ({
    wrapper: {
        width: '100%',
        height: '100%',
        overflow: 'auto',
        display: 'flex',
        justifyContent: 'center',
    },
    container: {
        height: 'fit-content',
        maxWidth: 'calc(100% - 96px)',
        margin: 48,
        padding: '32px 48px',
        overflow: 'hidden',
        display: 'flex',
        flexDirection: 'column',
        color: textColors.primary,
        fontFamily: fontFamilys.primary,
        backgroundColor: colors.white,
        borderRadius: borderRadiuses.primary,
        ...getShadowStyle({ color: boxShadows.primary }),
        '& p, span': {
            margin: '0 0 32px',
        },
        '& p': {
            fontSize: fontSizes.big,
        },
        '& button': {
            height: baseHeights.primary,
            width: 240,
            alignSelf: 'center',
            border: 'none',
            outline: 'none',
            cursor: 'pointer',
            borderRadius: borderRadiuses.normal,
            backgroundColor: colors.orange,
            color: textColors.white,
            ...cStyles.noneUserSelect,
            ...getShadowStyle({ color: boxShadows.primary }),
            '&:hover': {
                ...getShadowStyle({ color: boxShadows.primaryHover }),
            },
        },
        [theme.breakpoints.down('md')]: {
            maxWidth: 'calc(100% - 64px)',
            margin: 32,
            padding: '24px 32px',
            color: textColors.white,
            backgroundColor: colors.blue,
            '& p, span': {
                margin: '0 0 24px',
            },
        },
        [theme.breakpoints.down('sm')]: {
            maxWidth: 'calc(100% - 48px)',
            margin: 24,
            padding: '16px 24px',
            backgroundColor: colors.green,
            '& p, span': {
                margin: '0 0 16px',
            },
        },
        [theme.breakpoints.down('xs')]: {
            maxWidth: 'calc(100% - 32px)',
            margin: 16,
            padding: '8px 16px',
            backgroundColor: colors.red,
            '& p, span': {
                margin: '0 0 8px',
            },
        },
    },
    dataField: {
        marginTop: 32,
        minWidth: "90%",
        padding: 48,
        wordBreak: 'break-all',
        color: textColors.primary,
        borderRadius: borderRadiuses.regular,
        backgroundColor: colors.grayLight,
        [theme.breakpoints.down('md')]: {
            padding: 32,
            backgroundColor: colors.white,
        },
        [theme.breakpoints.down('sm')]: {
            padding: 24,
        },
        [theme.breakpoints.down('xs')]: {
            padding: 16,
        },
    },

// code training

    boxGif :{
        height : "auto",
        display : "flex",
        justifyContent : "center"
    },

    wrapBox : {
        marginTop : "-30px !important",
        display : "flex",
        justifyContent: 'space-between',
        flexWrap : "wrap",
        minHeight : "500px"
    },
    boxProduct : {
        borderRadius : 16,
        marginTop : 50,
        backgroundColor: colors.grayLight,
        width : "28%",
        '&:hover': {
            backgroundColor: colors.orange,
            color : textColors.white,
            "& p" : {
                backgroundColor: colors.blueDark
            }
        },
        [theme.breakpoints.down('md')]: {
            width : "48%",
        },
        [theme.breakpoints.down('sm')]: {
            width : "100%"
        },
        [theme.breakpoints.down('xs')]: {
           
        },
    },
    productImg : {
        width : "100%",
        height : 250,
        borderBottom : "6px",
        borderBottomStyle : "solid",
        borderBottomColor :colors.orange,
        borderTopLeftRadius: "16px",
        borderTopRightRadius : 16,
        [theme.breakpoints.down('md')]: {
           
        },
        [theme.breakpoints.down('sm')]: {
            height : "350px",
        },
        [theme.breakpoints.down('xs')]: {
           
        },
    },
    wrapContentProduct : {
        textAlign : "center",
        padding : "25px",
    },
    // overflow: hidden;
    // display: block;
    // display: -webkit-box;
    // -webkit-box-orient: vertical;
    // -webkit-line-clamp: 2;
    titleProduct : {
        fontSize : fontSizes.big,
        fontWeight: 600,
        lineHeight : 1.6,
        height : "70px !important",
        overflow: 'hidden',
        display : "-webkit-box",
        webkitBoxOrient : "vertical",
        webkitLineClamp : 2,
         [theme.breakpoints.down('md')]: {
           
        },
        [theme.breakpoints.down('sm')]: {
            minHeight : "auto !important",
        },
        [theme.breakpoints.down('xs')]: {
           
        },
    }, 
    desProduct : {
        marginTop : "10px",
        lineHeight : 1.6,
        height : "70px !important",
        overflow: 'hidden',
        display : "-webkit-box",
        "-webkitBoxOrient" : "vertical",
        "-webkitLineClamp" : 3,
        [theme.breakpoints.down('md')]: {
            
        },
        [theme.breakpoints.down('sm')]: {
            minHeight : "70px !important",
        },
        [theme.breakpoints.down('xs')]: {
           
        },
    }, 
    priceProduct : {
        margin : "15px",
        fontWeight: 600,
        fontSize : fontSizes.superBig,
    },
    productBuy: {
        fontSize : "15px !important",
        padding : "10px !important",
        borderRadius : 40,
        border: "2px",
        borderStyle : " solid",
        borderColor : colors.blue,
        fontWeight: 600,
    }
});