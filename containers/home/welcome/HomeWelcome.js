import React , {Suspense} from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';

// loading gif

import LoadingGif from "../../../assets/images/common/loading.gif"
// hocs
import withAuth from '../../../hocs/AuthHocs';

// actions
import SessionActions from '../../../reduxs/SessionRedux';
import UserActions from '../../../reduxs/UserRedux';
import RoleActions from '../../../reduxs/RoleRedux';
import ProductActions from "../../../reduxs/ProductsRedux"
// utils
import { getTokenContent } from '../../../utils/WebUtils';

// styles
import withStyles from '@material-ui/core/styles/withStyles';
import { styles } from './styles';

class HomeWelcome extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			classify: {
				user: 'userInfo',
				role: 'roles',
				products : "products"
			}
		};
	}

	componentDidMount() {
		const { classify } = this.state;
		this.props.getUserInfo(classify.user, { id: getTokenContent('contact_id') });
		this.props.getRoles(classify.role, { page: 1, size: 15 });
		this.props.getProduct(classify.products);
	}

	_renderUserInfo = () => {
		const { classify } = this.state;
		const { classes, userFetching, userContent } = this.props;
		return (
			<div className={classes.dataField}>
				{userFetching[classify.user] ? 'Loading...' : JSON.stringify(userContent[classify.user])}
			</div>
		)
	}

	_renderListRole = () => {
		const { classify } = this.state;
		const { classes, roleFetching, roleContent } = this.props;
		return (
			<div className={classes.dataField}>
				{roleFetching[classify.role] ? 'Loading...' : JSON.stringify(roleContent[classify.role])}
			</div>
		)
	}

	_renderLoading = ( classes) => {
		return (
			<>
				<div className={classes.boxGif}>
					<img src={LoadingGif} />
				</div>
			</>
		)
	}

	_renderProducts = (products , classes) => {
		return (
			<div className={classes.wrapBox}>
				{products && products.map(item => {
					return (
						<div key={item.id} className={classes.boxProduct}>
							<img className={classes.productImg} src={item.image} alt="item" />
							<div className={ classes.wrapContentProduct}>
								<div className={ classes.titleProduct}>{item.title}</div>
								<div className={classes.desProduct}>{item.description}
								</div>
								<div className={classes.priceProduct}>
									${item.price}
								</div>
								<p className={classes.productBuy}>MAKE A PROGRAM</p>
							</div>
						</div>
					)
				})}
			</div>
		)
	}
	_renderAllProduct = () => {
		const { classify } = this.state;
		const { classes, productFetching, productContent } = this.props;
		return (
			<div className={classes.dataField}>
				{productFetching[classify.products] ? this._renderLoading(classes) : this._renderProducts(productContent[classify.products], classes)}
			</div>
		)
	}

	render() {
		const { classes } = this.props;
		return (
			<div className={classes.wrapper}>
				<div className={classes.container}>
					<p>Welcome to this testing page!</p>
					<span>Đây là màn hình demo sau khi đăng nhập thành công, bấm đăng xuất để quay lại màn hình đăng nhập.</span>
					<button onClick={() => this.props.onLogout(true)}>Đăng xuất</button>
					{/* {this._renderUserInfo()} */}
					{/* {this._renderListRole()} */}
					{this._renderAllProduct()}
				</div>
			</div>
		);
	}

}

const mapStateToProps = state => {
	const { accessToken } = state.session;
	return {
		// session
		accessToken,
		// user
		userFetching: state.user.fetching,
		userContent: state.user.content,
		// role
		roleFetching: state.role.fetching,
		roleContent: state.role.content,
		// product  : 
		productFetching :state.products.fetching,
		productContent : state.products.content,
	};
}

const mapDispatchToProps = dispatch => ({
	// session
	onLogout: (isRedirect) => dispatch(SessionActions.sessionLogout(isRedirect)),
	// user
	getUserInfo: (classify, params) => dispatch(UserActions.getUserInfoRequest(classify, params)),
	// role
	getRoles: (classify, params) => dispatch(RoleActions.getRolesRequest(classify, params)),
	// products
	getProduct : (classify) => dispatch(ProductActions.getProductsRequest(classify))
});

export default compose(withAuth(), withStyles(styles), connect(mapStateToProps, mapDispatchToProps))(HomeWelcome);
