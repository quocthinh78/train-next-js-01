import { call, put, delay } from 'redux-saga/effects';

import ProductsActions from '../reduxs/ProductsRedux';

import { getDelayTime ,getTimestamp} from '../utils/DateUtils';
import {  getErrorMsg } from '../utils/StringUtils';

import ProductsApi from '../services/APIs/ProductsApi';

const api = new ProductsApi();

export function* getProducts(action) {
    console.log("productsata")
    const { classify } = action;
    const startReqAt = getTimestamp();
    try {
        let resp = yield call(api.getProducts)
        yield delay(getDelayTime(startReqAt, 'ms', 1500));
        console.log("resp",resp)
        yield put(ProductsActions.getProductsSuccess(classify , resp));
    } catch (error) {
        yield put(ProductsActions.getProductsFailure(classify, getErrorMsg(error)));
    }
}